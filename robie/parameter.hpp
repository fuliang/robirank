/*
 * Copyright (c) 2014 Hyokun Yun
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#ifndef __SOLAR_PARAMETER_HPP
#define __SOLAR_PARAMETER_HPP

#include "solar.hpp"

#include <iostream>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

namespace solar {

  using std::string;
  using std::cout;
  using std::cerr;
  using std::endl;
  using std::vector;

  struct parameter {

    string data_path_;
    double learning_rate_;
    double regularization_;
    int latent_dim_;

    int max_iteration_;
    int random_seed_;
    int xi_period_;

    int sample_num_;
    int test_row_num_;
    vector<unsigned int> test_levels_;

    string model_name_;
    ModelType model_type_;
    scalar cutoff_;
    bool flag_bold_;
    
    string loss_name_;
    LossType loss_type_;

    scalar sensitivity_;
    int wnum_;

    int numthreads_;
    int numtasks_;
    int rank_;

    string dump_prefix_;
    int dump_period_;

    int sgd_num_;

    int read_arguments(int argc, char **argv);

    friend std::ostream& operator<< (std::ostream& stream, const parameter& param) {
      stream << param.model_name_ << ","
             << param.latent_dim_ << "," << param.regularization_ << "," 
             << param.learning_rate_ << "," << param.xi_period_;
      return stream;
    }

  };
};

int solar::parameter::read_arguments(int argc, char **argv) {
  
  // GRAH: icc does not support vector initialization with an array, 
  // so this has to be done this way. argh!
  vector<unsigned int> default_test_levels;
  default_test_levels.push_back(1);
  default_test_levels.push_back(2);
  default_test_levels.push_back(3);
  default_test_levels.push_back(4);
  default_test_levels.push_back(5);
  default_test_levels.push_back(10);
  default_test_levels.push_back(100);
  default_test_levels.push_back(200);
  default_test_levels.push_back(500);

  // BUGBUG: fix experimental parameters
  flag_bold_ = false;
  sensitivity_ = 1.0;
  cutoff_ = 1.0;

  namespace po = boost::program_options;
  po::options_description desc("robie options:");
  desc.add_options()
    ("help", "produce help message")
    ("path", po::value<string>(&data_path_)->default_value("../Data/ml1m"), "path of data")
    ("nthreads", po::value<int>(&numthreads_)->default_value(4), "number of threads for each MPI process")
    ("lrate", po::value<double>(&learning_rate_)->default_value(0.001), "value of learning rate parameter")
    ("reg",  po::value<double>(&regularization_)->default_value(0.05), "value of regularization parameter")
    ("dim", po::value<int>(&latent_dim_)->default_value(100), "dimensionality of latent space")
    ("iter", po::value<int>(&max_iteration_)->default_value(1000), "number of iterations")
    ("seed", po::value<int>(&random_seed_)->default_value(12345), "random number generator seed value")
    ("period", po::value<int>(&xi_period_)->default_value(100), "period of xi variable updates")
    ("sam", po::value<int>(&sample_num_)->default_value(1000), 
     "number of samples to approximate the ranking loss")
    ("level", po::value<vector<unsigned int>>(&test_levels_)->multitoken()
     ->default_value( default_test_levels, "1,2,3,4,5,10,100,200,500"), 
     "levels precision values will be calculated")
    ("loss", po::value<string>(&loss_name_)->default_value("logistic"), 
     "name of the loss (logistic, hinge)")
    ("sgd_num", po::value<int>(&sgd_num_)->default_value(1), "number of stochastic gradients taken for each data point")
    ("dump_prefix", po::value<string>(&dump_prefix_)->default_value(""),
     "prefix to the parameter dump file")
    ("dump_period", po::value<int>(&dump_period_)->default_value(100),
     "how often parameters should be dumped")
    ;
  
  bool flagHelp = false;
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if(vm.count("help")){
    flagHelp = true;
  }
  
  if(flagHelp == true){
    cerr << desc <<endl;
    return 1;
  }
  
  if(data_path_.length() <= 0){
    cerr << "data file path not specified." <<endl;
    return 1;
  }

  if (loss_name_ == "logistic") {
    loss_type_ = LossType::LOGISTIC;
  }
  else if (loss_name_ == "hinge") {
    loss_type_ = LossType::HINGE;
  }
  else {
    cerr << "illegal loss name: " << loss_name_ << endl;
    return 1;    
  }

  return 0;
  
}


#endif
