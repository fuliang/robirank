/*
 * Copyright (c) 2014 Hyokun Yun
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#ifndef __SOLAR_SOLAR_HPP
#define __SOLAR_SOLAR_HPP

// BUGBUG: hacky redefinition. 
// in some systems, TBB does not recognize C++0x is available;
// is there a better way?
#undef TBB_IMPLEMENT_CPP0X
#define TBB_IMPLEMENT_CPP0X 1

#include <random>

#include "mpi.h"

#include "tbb/scalable_allocator.h"
#include "tbb/cache_aligned_allocator.h"
#include "tbb/mutex.h"
#include "tbb/spin_mutex.h"

/// type used for indices
namespace solar {
  using scalar = double;
  using index_type = int;
  using coordinate = std::pair<index_type, index_type>;

  using rng_type = std::mt19937_64;

  enum class ModelType{ LOGISTIC, SOLAR, WSABIE, HSOLAR, PWSABIE, ASOLAR, WSOLAR };
  enum class LossType{ LOGISTIC, HINGE };
}

#define MPI_SCALAR MPI_DOUBLE

template <typename T>
using sallocator = tbb::scalable_allocator<T>;

template <typename T>
using callocator = tbb::cache_aligned_allocator<T>;

using mutex_type = tbb::spin_mutex;
using lock_type = tbb::spin_mutex::scoped_lock;

#endif
