/*
 * Copyright (c) 2014 Hyokun Yun, Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#ifndef _LOADDATA_LIBSVM_HPP_
#define _LOADDATA_LIBSVM_HPP_

#include "appctx.hpp"

#include <vector>
#include <map>
#include <string>
using namespace std;

typedef struct{

  char data_path[PETSC_MAX_PATH_LEN];
  PetscInt dim;      // Dimensions 
  PetscInt m;        // Number of data points 
  PetscInt maxnnz;   // Maximum number of non-zeros in any row
  PetscInt maxlen;   // Maximum number of characters in any row 
  Vec      *labels;  // Store labels
  Mat      *data;    // Store data
  
  // nnz_array is used only for intermediate storage 
  PetscInt      *nnz_array;
  
  // Store group -> sample mapping (eg: query -> documents)
  map<PetscInt, vector<PetscInt> > *gs_map;

  // Store mapping between query id and query index 
  map<string, PetscInt> qid_idx_map;

  // Add an extra dimension?
  PetscBool add_bias;

  // Do indices begin with one in the LibSVM file?
  PetscBool one_offset;

} DataCtx;

PetscErrorCode parse_file(FILE* input,DataCtx* user);

PetscErrorCode parse_line(char* line,PetscInt* idxs,PetscScalar* vals,PetscScalar* label,PetscInt m,DataCtx* user);

PetscErrorCode assemble_matrix(FILE* input,PetscInt begin,PetscInt end,DataCtx* user);

PetscErrorCode fill_arrays_uni(FILE* input,DataCtx* user);

PetscErrorCode reparse_file(FILE* input,PetscInt* diag_nnz, PetscInt* offdiag_nnz,DataCtx* user);

PetscErrorCode fill_arrays_parallel(FILE* input,DataCtx* user);

PetscErrorCode LoadLibsvm(AppCtx *user_main);

#endif
